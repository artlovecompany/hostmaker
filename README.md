## Hostmaker
### Hostmaker Test Application
#### Author: Peter Fenyvesi

## Commands: 
- tsc - Compiles the project
- webpack - Compiles the react bundle
- npm test - Starts UnitTests
- npm start - Starts AppServer

## Configuration: 
- The configuration can be found here: ./config/default.json
- The setup sql file can be found here: ./config/setup.sql
- the configuration can be set via: --config | ex: --config=default | result: ./config/default.json
- the environment can be set via: --env | ex: --env=dev | result: ./config/default-env.json

## Urls: 
- (get) / - Is the index of the application
- (get) /setup - Will run the setup.sql file on the configured database. 
- (post) /edit - Will update the specified dataset.

## sample data: 
`
{ 
  "hostId": 1,
  "owner": "carlos",
  "address":
  { 
     "line1": "Flat 10",
     "line4": "7 Westbourne Terrace",
     "postCode": "W2 3UL",
     "city": "London",
     "country": "U.S" 
  }
}
`

## Notes:
- For this test i would have used Mongo, as the data is not relational. but as requested it has been made with postgres!
- For everything i decided to use Typescript & CommonJS as I love typescript, it gives structure, architecture, coding standards etc! Also i love to code in the same style|way on the front | back. 
- For the frontend i have decided to do a server side rendered react app, as the test requires little to none frontend jobs, still i didn't want to just put in plain dom elements, nor i wanted a client | server architecture. as its unecessary for the test. so now we only have to start the AppServer
- For the backend i have decided on Express, nodeJS;
- For testing i decided on Jasmine! as my favourite unittesting framework.
- As i am on Holidays i didn't had much time therefore, the UnitTest are only made for the Application and Database classes! 
