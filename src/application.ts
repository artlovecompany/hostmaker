import * as FS from 'fs';
import { Config } from './components/config';
import { Server } from './components/server';
import { Database } from './components/database';
import { Host } from './components/host';

import { ServerRender, Template } from './react';

export class Application
{
    server: Server = null;

    constructor()
    {
        this.server = new Server();
        this.createAPI();
    }

    createAPI()
    {
        this.server.application.get("/", this.requestIndex.bind(this));
        this.server.application.get("/setup", this.requestSetup.bind(this));
        this.server.application.post("/edit", this.requestEdit.bind(this));
    }

    //- Requests -//
    requestIndex(req, res)
    {
        var database = new Database();
        var list = [];

        database.query("SELECT * FROM HOSTS ORDER BY ID", [], (hosts, error) =>
        {
            if (error)
                return console.log(error);

            list.push(new Host({ id: -1, owner: "Owner", address: '{"line": "Address"}', income: "Income Generated" }));
            for (let i in hosts.rows)
                list.push(new Host(hosts.rows[i]));

            res.send(Template("Hostmaker", ServerRender(list)));
            database.close();
        });
    }

    requestEdit(req, res)
    {
        var data = req.body;
        if (!data.hostId || !data.owner || !data.address)
            return res.status(400).send("Invalid operation");

        var database = new Database();
        var change = new Date().toUTCString();

        data.address = JSON.stringify(data.address);
        database.query("SELECT * FROM HOSTS WHERE HOSTID=$1 LIMIT 1", [data.hostId], (original, error) =>
        {
            if (error)
                return this.invalidOperation(res, database);

            original = original.rows[0];
            database.query("UPDATE HOSTS SET ADDRESS=$1 VERSION=VERSION+1 WHERE ID=$2", [data.address, original.id], (updated, error) =>
            {
                if (error)
                    return this.invalidOperation(res, database);

                database.query("INSERT INTO HOSTS_HISTORY (HOSTID, VERSION, OWNER, CHANGED, ADDRESS, INCOME) VALUES ($1, $2, $3, $4, $5, $6)",
                    [original.hostId, original.version, original.owner, change, original.address, original.income], (history) =>
                    {
                        res.status(200).send("success");
                        database.close();
                    });
            });
        });
    }

    requestSetup(req, res)
    {
        var setup = FS.readFileSync("./config/setup.sql", 'utf8');
        var database = new Database();

        console.log(setup);
        database.query(setup, null, (hosts, error) =>
        {
            if (error)
                return console.log(error);

            res.status(200).send("success");
            database.close();
        });
    }

    //- helpers -//
    invalidOperation(res, database)
    {
        database.close();
        return res.status(400).send("Invalid operation");
    }
}