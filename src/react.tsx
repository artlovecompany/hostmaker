import * as React from 'react';
import * as ReactDom from 'react-dom';
import { renderToString } from 'react-dom/server';

export class App extends React.Component<any, {}>
{
    render()
    {
        return (
            <div id="HostContainer">
                {this.props.data.map((item) => <Host key={item.id} data={item} />)}
            </div>
        );
    }
}

export class Host extends React.Component<any, {}>
{
    constructor(props: any)
    {
        super(props);
        this.props.data.income = (!isNaN(parseFloat(this.props.data.income))) ? parseFloat(this.props.data.income).toFixed(2) + "£" : this.props.data.income;
    }

    render()
    {
        return (
            <div className="Host">
                <div className="data">{this.props.data.owner}</div>
                <div className="data">{Object.keys(this.props.data.address).map((key, index) => <Address key={index} data={this.props.data.address[key]} lineId={key} />)}</div>
                <div className="data">{this.props.data.income}</div>
            </div>
        );
    }
}

export class Address extends React.Component<any, {}>
{
    render()
    {
        return (
            <div className={"Line " + "lineId-" + this.props.lineId} >{this.props.data}</div>
        );
    }
}

export function ServerRender(data)
{
    return renderToString(<App data={data} />);
};

export function Template(title, body)
{
    return `
        <!DOCTYPE html>
        <html>
        <head>
            <title>${title}</title>
            <link rel="stylesheet" href="/dist/index.css" />
        </head>
        <body>
            <div id="root">${body}</div>
        </body>
        <script src="/dist/frontend.js"></script>
        </html>
        `;
}