import { Config } from './config';
import { Client } from 'pg';

export class Database
{
    client: any = null;
    constructor()
    {
        this.client = new Client(Config.database);
        this.client.connect();
    }

    query(query, data, callback)
    {
        this.client.query(query, data, (err, res) =>
        {
            callback(res, err);
        });
    }

    close()
    {
        this.client.end();
    }
}
