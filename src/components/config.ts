var Config;
import { existsSync } from 'fs';
import { resolve } from 'path';

export class ConfigComponent
{
    config: any;
    constructor()
    {
        var envName = this.parse('env');
        envName = (envName) ? envName : '';
        envName = (envName && envName !== '') ? '-' + envName : envName;

        var configName = this.parse('config');
        configName = (configName) ? configName : 'default';
        configName = "" + configName + envName + ".json";

        if (existsSync("./config/" + configName))
            this.config = require(resolve("./config/" + configName));
        else 
        {
            this.config = { error: 'Cannot find configuration:' + configName, code: 0 };
            console.log('Cannot find configuration:' + configName);
        }

        return this.config;
    }

    parse(name: string)
    {
        var accepted = ['--config', '--env'];
        for (let i in process.argv)
        {
            let argument = process.argv[i].split("=");
            if (accepted.indexOf(argument[0]) > -1 && argument[0].substring(2) === name)
                return (typeof argument[1] === 'undefined') ? true : argument[1];
        }
    }
}
Config = new ConfigComponent();
export { Config }