import { Config } from './config';
import * as FS from 'fs';
import * as HTTP from 'http';
import * as Express from 'express';
import * as BodyParser from 'body-parser';

export class Server
{
    application: any = null;
    server: any = null;

    constructor()
    {
        this.application = Express();
        this.application.use('/dist', Express.static('dist'));
        this.application.use(BodyParser.json());
        this.application.disable('x-powered-by');
        this.application.use(function (req, res, next)
        {
            res.removeHeader('x-powered-by');
            res.header('Application', "Hostmaker");
            next();
        });

        this.server = (this.application) ? HTTP.createServer(this.application) : HTTP.createServer();
        this.server.listen(Config.server.port, () =>
        {
            var host = (this.server.address().address == '::') ? '127.0.0.1' : this.server.address().address;
            var port = this.server.address().port;
            var family = this.server.address().family;

            console.log("Hostmaker application started. listening on: //%s:%s | %s", host, port, family);
        });
    }
}