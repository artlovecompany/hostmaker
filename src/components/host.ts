export class Host
{
    resolved: any = null;
    constructor(data)
    {
        this.resolved = {
            id: data.id,
            hostId: data.hostid,
            version: data.version,
            changed: data.changed,
            owner: data.owner,
            address: JSON.parse(data.address),
            income: data.income
        };
        return this.resolved;
    }
}