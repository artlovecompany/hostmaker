import { Database } from './database';

describe("Database Tests: ", () =>
{
    var database = new Database();
    it("Should connect", (done) =>
    {
        expect(database).toBeDefined();
        database.query("CREATE TABLE UNIT_TEST (TEST INT)", null, (response, error) =>
        {
            expect(error).toBeFalsy();
            done();
        });
    });

    it("Should query", (done) =>
    {
        database.query("INSERT INTO UNIT_TEST VALUES(1)", null, (insert, error) =>
        {
            expect(error).toBeFalsy();
            database.query("SELECT * FROM UNIT_TEST", null, (select, error) =>
            {
                expect(select.rowCount).toBe(1);
                expect(select.rows[0].test).toBe(1);
                done();
            });
        });
    });

    afterAll((done) =>
    {
        database.query("DROP TABLE UNIT_TEST", null, (response, error) =>
        {
            done();
        });
    });
});