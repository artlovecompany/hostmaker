import { Application } from './application';
import * as request from 'request';

const request = require("request");
const serverUrl = "http://127.0.0.1:9850";

describe("Application Tests: ", () =>
{
    var HostmakerApp = new Application();
    HostmakerApp.server.application.get("/test", (req, res) =>
    {
        res.status(200).send("success");
    });

    it("Should run the server", (done) =>
    {
        expect(HostmakerApp).toBeDefined();
        expect(HostmakerApp.server).toBeDefined();
        done();
    });

    it("Should accept requests", (done) =>
    {
        request.get(serverUrl + "/test", function (error, response, body)
        {
            expect(response.statusCode).toBe(200);
            expect(body).toBe("success");
            done();
        });
    });

});