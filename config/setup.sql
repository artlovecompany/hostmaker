--DROP TABLE HOSTS;
--DROP TABLE HOSTS_HISTORY;

CREATE TABLE HOSTS (
   ID SERIAL PRIMARY KEY,
   HOSTID INT NOT NULL,
   VERSION 		  INT  NOT NULL,
   CHANGED        timestamp,
   OWNER          TEXT NOT NULL,
   ADDRESS        TEXT,
   INCOME         FLOAT
);

CREATE TABLE HOSTS_HISTORY (
   ID SERIAL PRIMARY KEY,
   HOSTID INT NOT NULL,
   VERSION 		  INT  NOT NULL,
   CHANGED        timestamp,
   OWNER          TEXT NOT NULL,
   ADDRESS        TEXT,
   INCOME         FLOAT
);

INSERT INTO HOSTS (HOSTID, VERSION, OWNER, CHANGED, ADDRESS, INCOME) VALUES(1, 0, 'carlos', NOW(),  '{"line1": "Flat 5","line4": "7 Westbourne Terrace","postCode": "W2 3UL","city": "London","country": "U.K."}', 2000.34);
INSERT INTO HOSTS (HOSTID, VERSION, OWNER, CHANGED, ADDRESS, INCOME) VALUES(2, 0, 'ankur', NOW(), '{"line1": "4","line2": "Tower Mansions","line3": "Off Station road","line4": "86-87 Grange Road","postCode": "SE1 3BW","city": "London","country": "U.K."}', 1000);
INSERT INTO HOSTS (HOSTID, VERSION, OWNER, CHANGED, ADDRESS, INCOME) VALUES(3, 0, 'elaine', NOW(), '{"line1": "4","line2": "332b","line4": "Goswell Road","postCode": "EC1V 7LQ","city": "London","country": "U.K."}', 1200);